export class Constants {
  static authority = 'http://localhost:5000/';
  static clientId = 'js';
  static clientRoot = 'http://localhost:3000/';
  static clientScope = 'openid profile simple_api';
  static apiRoot = 'http://localhost:6001/';
}
