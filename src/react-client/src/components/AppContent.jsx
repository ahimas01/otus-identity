import * as React from 'react';

import {ApiService} from '../services/ApiService';
import {AuthService} from '../services/AuthService';

import AuthContent from './AuthContent';
import Buttons from './Buttons';

export default class AppContent extends React.Component {
  authService;
  apiService;
  shouldCancel;
  state = {api: {}, user: {}};

  constructor(props) {
    super(props);

    this.authService = new AuthService();
    this.apiService = new ApiService();
    this.state = {user: {}, api: {}};
    this.shouldCancel = false;
  }

  componentDidMount() {
    this.getUser();
  }

  login = () => {
    this.authService.login();
  };

  callApi = () => {
    this.apiService
        .callApi()
        .then(data => {
          this.setState({api: data.data});
          console.log('Api return successfully data, check in section - Api response');
        })
        .catch(error => {
          console.error(error);
        });
  };

  componentWillUnmount() {
    this.shouldCancel = true;
  }

  renewToken = () => {
    this.authService
        .renewToken()
        .then(user => {
          console.log('Token has been sucessfully renewed. :-)');
          this.getUser();
        })
        .catch(error => {
          console.error(error);
        });
  };

  logout = () => {
    this.authService.logout();
  };

  getUser = () => {
    this.authService.getUser().then(user => {
      if (user) {
        console.log('User has been successfully loaded from store.');
      } else {
        console.log('You are not logged in.');
      }

      if (!this.shouldCancel) {
        this.setState({user});
      }
    });
  };

  render() {
    return (
        <>
          <Buttons
              login={this.login}
              logout={this.logout}
              renewToken={this.renewToken}
              getUser={this.getUser}
              callApi={this.callApi}
          />

          <AuthContent api={this.state.api} user={this.state.user}/>
        </>
    );
  }
}
