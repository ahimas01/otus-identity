import * as React from 'react';
import JSONTree from 'react-json-tree';

export default class JsonTreeViewer extends React.Component {
    renderJsonData() {
        return (this.props.data && this.props.data.length !== 0) ? (
            <>
                <h1>{this.props.title}</h1>
                <JSONTree data={this.props.data} theme="bright" shouldExpandNode={this.props.shouldExpandNode}/>
            </>
        ) : null;
    }

    render() {
        return this.renderJsonData();
    }
}
